﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ZCPPFileDump
{
    class PortString
    {
        public PortString (byte[] content, int offset)
        {
            pixelPort = (content[offset] & 0x80) == 0;
            port = content[offset] & 0x7F;

            smartRemote = (content[offset+1] & 0xC0) >> 6;
            portString = content[offset + 1] & 0x3F;
            protocol = content[offset + 2];
            grouping = content[offset + 3];

            startChannel = (((int)content[offset + 4]) << 24) + (((int)content[offset + 5]) << 16) + (((int)content[offset + 6]) << 8) + (int)content[offset + 7];
            channels = (((int)content[offset + 8]) << 24) + (((int)content[offset + 9]) << 16) + (((int)content[offset + 10]) << 8) + (int)content[offset + 11];
            forward = (content[offset + 12] & 0x80) == 0;
            colourOrder = content[offset + 12] & 0x07;
            nullPixels = content[offset + 13];
            brightness = content[offset + 14];
            gamma = (float)content[offset + 15] / 10;
        }

        public void AddName(byte[] content, int offset)
        {
            int len = content[offset + 2];
            for (int i = 0; i < len; i++)
            {
                if (content[offset + 3 + i] != 0x00)
                {
                    name += (char)content[offset + 3 + i];
                }
            }
        }

        public void Dump()
        {
            if (pixelPort)
            {
                Console.Write("    Pixel Port ");
            }
            else
            {
                Console.Write("    Serial Port ");
            }
            Console.Write((port+1).ToString() + " string " + (portString+1).ToString());
            if (smartRemote != 0)
            {
                Console.Write(" Smart String " + smartRemote.ToString());
            }
            Console.Write(" Start " + (startChannel+1).ToString() + " Channels " + channels.ToString() + " ");
            Console.Write(Protocol());
            if (grouping > 1)
            {
                Console.Write(" Grouping " + grouping.ToString());
            }
            if (!forward)
            {
                Console.Write(" Reversed");
            }
            Console.Write(" " + ColourOrder());
            if (nullPixels != 0)
            {
                Console.Write(" Null Pixels " + nullPixels.ToString());
            }
            Console.Write(" Brightness " + brightness.ToString() + "%");
            if (gamma != 1.0)
            {
                Console.Write(" Gamma " + gamma.ToString());
            }
            Console.WriteLine(" '" + name + "'");
        }

        public bool Is(bool _serial, int _port, int _smartRemote, int _stringNum)
        {
            return _serial != pixelPort && _port == port && smartRemote == _smartRemote && portString == _stringNum;
        }

        public bool pixelPort;
        public string name;
        public int port;
        public int smartRemote;
        public int portString;
        public int startChannel;
        public int channels;
        public int protocol;
        public int grouping;
        public bool forward;
        public int colourOrder;
        public int nullPixels;
        public int brightness;
        public float gamma;

        string ColourOrder()
        {
            switch (colourOrder)
            {
                case 0x00: return "RGB";
                case 0x01:
                    return "RBG";
                case 0x02:
                    return "GRB";
                case 0x03:
                    return "GBR";
                case 0x04:
                    return "BRG";
                case 0x05: return "BGR";
            }

            return "Unknown";
        }
        string Protocol()
        {
            switch(protocol)
            {
                case 0x00: return "WS2811";
                case 0x01: return "GECE";
                case 0x02: return "DMX";
                case 0x03: return "LX1203";
                case 0x04: return "TLS3001";
                case 0x05: return "LPD6803";
                case 0x06: return "WS2801";
                case 0x07: return "SM16716";
                case 0x08: return "MB16020";
                case 0x09: return "MY92131";
                case 0x0A: return "APA102";
                case 0x0B: return "MY9221";
                case 0x0C: return "SK6812";
                case 0x0D: return "UCS1903";
                case 0x0E: return "TM18XX";
                case 0x0F: return "RENARD";
                case 0x10: return "LPD8806";
                case 0x11: return "DM412";
                case 0x12: return "P9813";
                case 0x13: return "LOR";
            }
            return "Unknown";
        }
    }

    class Controller
    {
        public int sequence;
        public string userName;
        public int priority;
        public List<PortString> strings = new List<PortString>();
        public Controller() { }

        public void Dump()
        {
            Console.WriteLine("Controller '" + userName + "' sequence " + sequence.ToString() + " priority " + priority.ToString());
            foreach (var p in strings)
            {
                p.Dump();
            }
        }
        public void ProcessNames(byte[] content, int offset)
        {
            int ports = content[offset + 11];
            int current = offset + 12;
            for (int i = 0; i < ports; i++)
            {
                bool serial = (content[current] & 0x80) != 0;
                int port = content[current] & 0x7F;
                int smartRemote = (content[current + 1] & 0xC0) >> 6;
                int stringNum = content[current + 1] & 0x3F;
                int len = content[current + 2];

                foreach(var p  in strings)
                {
                    if (p.Is(serial, port, smartRemote, stringNum))
                    {
                        p.AddName(content, current);
                    }
                }

                current += 3 + len;
            }
        }

        public void ProcessConfig(byte[] content, int offset)
        {
            sequence = (int)content[offset + 6] << 8 + content[offset + 7];
            for (int i = 0; i < 32; i++)
            {
                if (content[offset + 8 + i] == 0x00) break;
                userName += (char)content[offset + 8 + i];
            }
            priority = content[offset + 41];
            int ports = content[offset + 43];
            for (int i = 0; i < ports; i++)
            {
                strings.Add(new PortString(content, offset + 44 + i * 16));
            }
        }
    }

    class Program
    {
        static void ProcessDirectory(string dir)
        {
            DirectoryInfo di = new DirectoryInfo(dir);

            foreach(var d in di.GetDirectories())
            {
                ProcessDirectory(d.FullName);
            }
            foreach (var f in di.GetFiles("*.zcpp"))
            {
                ProcessFile(f.FullName);
            }
        }

        static void ProcessFile(string file)
        {
            if (!File.Exists(file)) return;
            FileInfo fi = new FileInfo(file);
            if (fi.Extension.ToLower() != ".zcpp") return;

            BinaryReader br = new BinaryReader(new StreamReader(file).BaseStream);
            byte[] content = br.ReadBytes(999999999);
            br.Close();

            if (content[0] != 'Z' || content[1] != 'C' || content[2] != 'P' || content[3] != 'P') return;

            Console.WriteLine();
            Console.WriteLine("ZCPP File found. IP: " + fi.Name);

            int offset = 4;

            Controller controller = new Controller();

            while (offset < content.Length)
            {
                int type = content[offset];
                if (type == 0xFF)
                {
                    controller.Dump();
                    return;
                }
                int size = (((int)content[offset + 1]) << 8) + content[offset + 2];
                if (type == 0x00)
                {
                    controller.ProcessConfig(content, offset + 3);
                }
                else if (type == 0x01)
                {
                    controller.ProcessNames(content, offset + 3);
                }
                else
                {
                    Console.WriteLine("Unknown packet type: " + type.ToString());
                }
                offset += 3 + size;
            }
            Console.WriteLine("Unexpected end of file.");
            controller.Dump();
        }

        static void Main(string[] args)
        {
            foreach (var s in args)
            {
                if (Directory.Exists(s))
                {
                    ProcessDirectory(s);
                }
                else
                {
                    ProcessFile(s);
                }
            }
            Console.WriteLine();
            Console.WriteLine("Hit enter to exit.");
            Console.ReadLine();
        }
    }
}
